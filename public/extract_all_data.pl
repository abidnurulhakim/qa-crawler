# Author
# Name  : Abid Nurul Hakim
# NPM   : 1206237214
# Date  : 14/02/2016
# ===============================================

use strict;
use Data::Dumper;
use DateTime;

main();
sub main
{
    my @resultRandomQA;

    my @nameFileTanyadok = ('crawl/tanyadok_2016-02-14-150022_texts.txt');
    my @nameFileAlodokter = ('crawl/alodokter_demam_berdarah_2016-02-15-104531_texts.txt', 'crawl/alodokter_ispa_2016-02-15-104539_texts.txt', 'crawl/alodokter_malaria_2016-02-15-104535_texts.txt', 'crawl/alodokter_tuberkolosis_2016-02-15-104537_texts.txt');
    my @nameFileKlikdokter = ('crawl/klikdokter_campak_2016-02-15-104606_texts.txt', 'crawl/klikdokter_anemia_2016-02-15-104615_texts.txt', 'crawl/klikdokter_kanker_2016-02-15-104619_texts.txt', 'crawl/klikdokter_tht_2016-02-15-104556_texts.txt');

    my @resultRandomQAAlodokter;
    my %resultRandomQAAlodokterID;
    for (my $x = 0; $x < scalar(@nameFileAlodokter); $x++) {
        my @resultTemp = extractorAlodokter($nameFileAlodokter[$x]);
        foreach my $element (@resultTemp) {
            push(@resultRandomQAAlodokter, $element);
        }
    }
    my $x = 0;
    while ($x < 100 && (scalar(@resultRandomQAAlodokter) > scalar(keys(%resultRandomQAAlodokterID))) ) {
        my $index = int(rand(scalar(@resultRandomQAAlodokter)));
        if (!$resultRandomQAAlodokterID{$index}) {
            push(@resultRandomQA, $resultRandomQAAlodokter[$index]);
            $resultRandomQAAlodokterID{$index} = 1;
        }
        $x++;
    }

    my @resultRandomQAKlikdokter;
    my %resultRandomQAKlikdokterID;
    for (my $x = 0; $x < scalar(@nameFileKlikdokter); $x++) {
        my @resultTemp = extractorKlikdokter($nameFileKlikdokter[$x]);
        foreach my $element (@resultTemp) {
            push(@resultRandomQAKlikdokter, $element);
        }
    }
    my $x = 0;
    while ($x < 100 && (scalar(@resultRandomQAKlikdokter) > scalar(keys(%resultRandomQAKlikdokterID))) ) {
        my $index = int(rand(scalar(@resultRandomQAKlikdokter)));
        if (!$resultRandomQAKlikdokterID{$index}) {
            push(@resultRandomQA, $resultRandomQAKlikdokter[$index]);
            $resultRandomQAKlikdokterID{$index} = 1;
        }
        $x++;
    }

    my $dt   = DateTime->now;   # Stores current date and time as datetime object
    my $date = $dt->ymd;   # Retrieves date as a string in 'yyyy-mm-dd' format
    my $time = $dt->hms;   # Retrieves time as a string in 'hh:mm:ss' format
    open(OUT, '>', 'resultQA_'.$date.'_'.$time.'.txt');
    print OUT Dumper(\@resultRandomQA);
    close(OUT);
}

sub extractorAlodokter
{
    my($inputFileName, $outputFileName) = (@_[0], @_[1]);
    open(inAlodokter, "<", $inputFileName);
    if ($outputFileName) {
        open(outAlodokter, ">", $outputFileName);
    }
    my $data = "";
    while (my $line = <inAlodokter>)
    {
        $data .= $line;
    }
    close(inAlodokter);

    my @result;
    my $question;
    my $answer;
    my @replies;
    my @tempArray;
    my @articles = getTagValue("article", $data);
    for (my $x = 0; $x < scalar(@articles); $x++) {
        $question = "";
        $answer = "";
        @replies = split("<div class=\"alodokter-bbp-content-reply-", $articles[$x]);
        if (scalar(@replies) > 2) {
            @tempArray = split(">", $replies[1], 2);
            @tempArray = split("</div>", $tempArray[1], 2);
            $question = removeAllTagHTML($tempArray[0]);

            @tempArray = split(">", $replies[2], 2);
            @tempArray = split("</div>", $tempArray[1], 2);
            $answer = removeAllTagHTML($tempArray[0]);
            my %temp;
            $temp{'question'} = $question;
            $temp{'answer'} = $answer;
            push(@result, \%temp);
        }
    }
    if ($outputFileName) {
        print outAlodokter Dumper(\@result);
        close(outAlodokter);
    }
    
    return @result;
}

sub extractorTanyadok
{
    my($inputFileName, $outputFileName) = (@_[0], @_[1]);
    open(inTanyadok, "<", $inputFileName);
    if ($outputFileName) {
        open(outTanyadok, ">", $outputFileName);
    }
    my $data = "";
    while (my $line = <IN>)
    {
        $data .= $line;
    }
    close(inTanyadok);

    my @result;
    my @articles = getTagValue("article", $data);
    for (my $x = 0; $x < scalar(@articles); $x++) {
        my $question;
        my @title = getTagValue("h1", $articles[$x]);
        $question .= $title[0];
        my @tagAnswer = split("<div id=\"qa-archive-answer\">\n+<p><strong>Jawaban", $articles[$x]);
        my @tempSplit = split("<p>", $tagAnswer[0]);
        for (my $var = 1; $var < scalar(@tempSplit); $var++) {
            if (length($tempSplit[$var]) > 0 && $tempSplit[$var] ne " ") {
                my @tempQuestion = split("</p>", $tempSplit[$var]);
                $tempQuestion[0] = removeAllTagHTML($tempQuestion[0]);
                $question .= ". ".$tempQuestion[0];
            }
        }
        my @answer = getTagValue("p", $tagAnswer[1]);
        $answer[0] = removeAllTagHTML($answer[0]);
        my %temp;
        $temp{'question'} = $question;
        $temp{'answer'} = $answer[0];
        push(@result, \%temp);
    }
    if ($outputFileName) {
        print outTanyadok Dumper(\@result);
        close(outTanyadok);
    }
    
    return @result;
}

sub extractorKlikdokter
{
    my($inputFileName, $outputFileName) = (@_[0], @_[1]);
    open(inKlikdokter, "<", $inputFileName);
    if ($outputFileName) {
        open(outKlikdokter, ">", $outputFileName);
    }    
    my $data = "";
    while (my $line = <inKlikdokter>)
    {
        $data .= $line;
    }
    close(inKlikodokter);

    my @result;
    my $question;
    my $answer;
    my @tempArray;
    my @docs = split("<div class=\"pertanyaan-bg\">", $data);
    if (scalar(@docs) > 2) {
        for (my $x = 1; $x < scalar(@docs); $x++) {
            $question = "";
            $answer = "";
            @tempArray = split("</div>", $docs[$x], 2);
            $question = removeAllTagHTML($tempArray[0]);

            @tempArray = split("<div class=\"at-3\">", $tempArray[1], 2);
            @tempArray = split("</div>", $tempArray[1], 2);
            $answer = removeAllTagHTML($tempArray[0]);
            my %temp;
            $temp{'question'} = $question;
            $temp{'answer'} = $answer;
            push(@result, \%temp);
        }
        
    }
    if ($outputFileName) {
        print outKlikdokter Dumper(\@result);
        close(outKlikdokter);
    }
    return @result;
}

# Get value from tag in string.
# @param $ (scalar)
# @param $ (scalar)
# @return @ (array)
sub getTagValue
{
    my @result;
    my($stringTag, $stringData) = (@_[0], @_[1]);
    my @tempSplit = split("<$stringTag [^>]*>", $stringData);
    if (scalar(@tempSplit) < 2) {
        @tempSplit = split("<$stringTag>", $stringData);
        if (scalar(@tempSplit) < 2) {
            return @result;
        }
    }
    for (my $var = 1; $var < scalar(@tempSplit); $var++) {
        if (length($tempSplit[$var]) > 0 && $tempSplit[$var] ne " ") {
            my @temp = split("</$stringTag>", $tempSplit[$var]);
            $temp[0] =~ s/^\s+|\s+$//g;
            push(@result, $temp[0]);
        }
    }
    return @result;
}

# Remove all tag HTML in string.
# @param $ (scalar)
# @return $ (scalar)
sub removeAllTagHTML
{
    my $result = @_[0];
    $result =~ s/<br>//g;
    $result =~ s/<li>\n*/- /g;
    $result =~ s/<[^>]*>/ /g;
    $result =~ s/[[:^ascii:]]+/ /g;
    $result =~ s/^\s+|\s+$//g;
    $result =~ s/&nbsp;//g;
    return $result;
}