# Author
# Name  : Abid Nurul Hakim
# NPM   : 1206237214
# Date  : 14/02/2016
# ===============================================

use strict;
use Data::Dumper;

main();

sub main {
    open(IN, "<", "crawl/alodokter_tuberkolosis_2016-02-15-104537_texts.txt");
    open(OUT, ">", "console_alodokter_tuberkolosis.txt");
    my $data = "";
    while (my $line = <IN>)
    {
        $data .= $line;
    }
    close(IN);

    my @result;
    my $question;
    my $answer;
    my @replies;
    my @tempArray;
    my @articles = getTagValue("article", $data);
    for (my $x = 0; $x < scalar(@articles); $x++) {
        $question = "";
        $answer = "";
        @replies = split("<div class=\"alodokter-bbp-content-reply-", $articles[$x]);
        if (scalar(@replies) > 2) {
            @tempArray = split(">", $replies[1], 2);
            @tempArray = split("</div>", $tempArray[1], 2);
            $question = removeAllTagHTML($tempArray[0]);

            @tempArray = split(">", $replies[2], 2);
            @tempArray = split("</div>", $tempArray[1], 2);
            $answer = removeAllTagHTML($tempArray[0]);
            my %temp;
            $temp{'question'} = $question;
            $temp{'answer'} = $answer;
            push(@result, \%temp);
        }
    }
    print OUT Dumper(\@result);
}

# Get value from tag in string.
# @param $ (scalar)
# @param $ (scalar)
# @return @ (array)
sub getTagValue
{
    my @result;
    my($stringTag, $stringData) = (@_[0], @_[1]);
    my @tempSplit = split("<$stringTag [^>]*>", $stringData);
    if (scalar(@tempSplit) < 2) {
        @tempSplit = split("<$stringTag>", $stringData);
        if (scalar(@tempSplit) < 2) {
            return @result;
        }
    }
    for (my $var = 1; $var < scalar(@tempSplit); $var++) {
        if (length($tempSplit[$var]) > 0 && $tempSplit[$var] ne " ") {
            my @temp = split("</$stringTag>", $tempSplit[$var]);
            $temp[0] =~ s/^\s+|\s+$//g;
            push(@result, $temp[0]);
        }
    }
    return @result;
}

# Remove all tag HTML in string.
# @param $ (scalar)
# @return $ (scalar)
sub removeAllTagHTML
{
    my $result = @_[0];
    $result =~ s/<br>//g;
    $result =~ s/<[^>]*>/ /g;
    $result =~ s/[[:^ascii:]]+/ /g;
    $result =~ s/^\s+|\s+$//g;
    return $result;
}